import { Injectable } from '@angular/core';
import ActivitiesData from '../../assets/ActivitiesData.json';
import { Subject } from 'rxjs';
import { ActivityFilter } from './activity-filter.model';
import { Router } from '@angular/router';
import Weekdays from '../../assets/Weekdays.json';
import { Activity } from '.././models/Activity';

@Injectable({
  providedIn: 'root',
})
export class ActivityService {
  activitiesData: any = ActivitiesData;
  editDisplayedActivity: boolean = false;
  // activitiesData: Activity[] = ActivitiesData
  displayedActivityDetails: Activity;
  private filteredActivitiesByDescription = new Subject<Activity[]>();
  private filteredActivitiesByGeoArea = new Subject<Activity[]>();
  private filteredActivitiesByInterests = new Subject<Activity[]>();
  private filteredActivitiesByDate = new Subject<Activity[]>();
  private toggleActivityFilterMenu = new Subject<Boolean>();

  constructor(private router: Router) {}

  getFilteredActivitiesByDescription() {
    return this.filteredActivitiesByDescription.asObservable();
  }
  getFilteredActivitiesByGeoArea() {
    return this.filteredActivitiesByGeoArea.asObservable();
  }
  getFilteredActivitiesByInterests() {
    return this.filteredActivitiesByInterests.asObservable();
  }
  getFilteredActivitiesByDate() {
    return this.filteredActivitiesByDate.asObservable();
  }
  getToggleActivityFilterMenu() {
    return this.toggleActivityFilterMenu.asObservable();
  }

  toggleMenu(bool = false) {
    this.toggleActivityFilterMenu.next(bool);
  }

  // async for future plan to use api
  async filterActivities(filter: ActivityFilter) {
    try {
      //filter by searchText
      const filterBySearchText = this.activitiesData.filter((activity) =>
        activity.description.includes(filter.searchText)
      );
      this.filteredActivitiesByDescription.next(filterBySearchText);

      //filter by date
      const filterByDate = this.activitiesData.filter((activity) => {
        const targetDate = new Date(filter.selectedDate);
        const { startDate, endDate } = activity;
        const earlyDate = new Date(startDate);
        const latestDate = new Date(endDate);

        return (
          targetDate.getTime() >= earlyDate.getTime() &&
          targetDate.getTime() <= latestDate.getTime()
        );
      });
      this.filteredActivitiesByDate.next(filterByDate);

      //filter by geoArea
      // send {geoArea,range} to server and set filteredActivitiesByGeoArea
      // for now,just plain no filter
      this.filteredActivitiesByGeoArea.next(this.activitiesData);

      // filter by interests
      const filterByInterests = this.activitiesData.filter((activity) =>
        activity.interests.includes(filter.interests)
      );
      this.filteredActivitiesByInterests.next(filterByInterests);

      return { error: null, isSuccess: true };
    } catch (error) {
      console.log(
        '🚀 ~ file: activity.service.ts ~ line 39 ~ ActivityService ~ filterActivities ~ error',
        error
      );
      return { error: error, isSuccess: false };
    }
  }

  updateActivity(activity: Activity) {}

  deleteActivity() {
    let index = this.activitiesData.findIndex(
      (activity) => activity._id == this.displayedActivityDetails._id
    );
    console.log('index', index);
    console.log(this.activitiesData.length);
    this.activitiesData.splice(index, 1);
    console.log(this.activitiesData.length);
    alert('הפעילות נמחקה');
    this.router.navigateByUrl(`/Home`);
  }

  createNewActivity(activity: Activity) {}

  navToActivity(id: string) {
    console.log(`entered - ${id}`);
    this.loadActivity(id);
    this.displayedActivityDetails = this.activitiesData.find(
      (activity) => activity._id == id
    );
    this.router.navigateByUrl(`/activity-details/${id}`);
  }

  loadActivity(id: string) {
    this.displayedActivityDetails = this.activitiesData.find(
      (activity) => activity._id == id
    );
  }

  toPrettyDate(date: string) {
    let activityDate = new Date(date);
    let day = activityDate.getDate();
    let month = activityDate.getMonth() + 1;
    let weekDay = Weekdays[activityDate.getDay()];
    return `${(day > 9 ? '' : '0') + day}/${
      (month > 9 ? '' : '0') + month
    }, ${weekDay}`;
  }
}
