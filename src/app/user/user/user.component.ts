import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/activity/activity.service';
import { Activity } from 'src/app/models/Activity';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  activities: Activity[];
  location: string = 'רמת גן';
  age: number = 24;
  labels: string[] = ['ספורט', 'ים'];
  futureActivities = [
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'ציור ויצירה',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'ציור ויצירה',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'ציור ויצירה',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
  ];
  pastActivities = [
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'אימון',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'אימון',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'אימון',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'אימון',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
    {
      imgSrc: 'assets/icons/activity.jpg',
      name: 'אימון',
      location: 'בן יהודה 71, תל אביב',
      date: '30.06 || 12:00',
    },
  ];

  constructor(private _location: Location, private router: Router, private activitiesService: ActivityService) {
    this.activities = activitiesService.activitiesData
    console.log("user ac",this.activities);

  }

  ngOnInit(): void {}

  goBack() {
    this._location.back();
  }
  navToProfileEdit(){
    this.router.navigateByUrl("/profile-edit")
  }
}
