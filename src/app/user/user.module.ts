import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user/user.component';
import { ActivityModule } from '../activity/activity.module';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';

@NgModule({
  declarations: [UserComponent, ProfileEditComponent],
  imports: [CommonModule, ActivityModule],
  exports: [UserComponent],
})
export class UserModule {}
