import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PickLocationComponent } from './pick-location/pick-location.component';



@NgModule({
  declarations: [
    PickLocationComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DialogsModule { }
